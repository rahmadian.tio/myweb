from django.shortcuts import render
from django.shortcuts import redirect
from django.http import HttpResponseRedirect
from .models import Status
from .forms import formStatus

# Create your views here.
response = {'author' : 'Rahmadian Tio Pratama'}

def home(request):
	return render(request, 'Web.html')

def landpage(request):
	statusRes = Status.objects.all()
	response['statusRes'] = statusRes
	response['form_status'] = formStatus
	return render(request, "landpage.html", response)
	
def post(request):
	form_status = formStatus(request.POST or None)
	if(request.method == 'POST' and form_status.is_valid()):
		response['status'] = request.POST['status']
		status = Status(status = response['status'])
		status.save()
		return HttpResponseRedirect('/')
	
	else:
		return HttpResponseRedirect('/')