from django.test import TestCase, Client
from django.urls import resolve, reverse
from django.http import HttpRequest
from selenium import webdriver
from selenium.webdriver.chrome.options import Options
from .forms import formStatus
from .views import landpage, post
from .models import Status
import unittest
import time

# Create your tests here.
class TestTDD(TestCase):
	def test_landpage_exist(self):
		response = Client().get('/')
		self.assertEqual(response.status_code,200)
		
	def test_using_template(self):
		response = Client().get('/')
		self.assertTemplateUsed(response, 'landpage.html')
		
	def test_home_exist(self):
		response = Client().get('/home/')
		self.assertEqual(response.status_code, 200)
		self.assertTemplateUsed(response, 'Web.html')
		
	def test_content_hello(self):
		request = HttpRequest()
		response = landpage(request)
		htmlResponse = response.content.decode('utf8')
		self.assertIn('Hello, Apa kabar?', htmlResponse)
		
	def test_using_func(self):
		found = resolve('/Website')
		self.assertEqual(found.func, landpage)
		
	def test_create_new_status(self):
		statusBaru = Status.objects.create(status = 'Test Status')
		hitung_semua_status = Status.objects.all().count()
		self.assertEqual(hitung_semua_status, 1)
		
	def test_form_validation_blank_items(self):
		form = formStatus(data = {'status' : ''})
		self.assertFalse(form.is_valid())
		self.assertEqual(form.errors['status'], ["This field is required."])
	
	def test_post_success_and_render_result(self):
		test = '<!DOCTYPE html>'
		response_post = Client().post('/', {'status' : test})
		self.assertEqual(response_post.status_code, 200)

		response = Client().get('/')
		html_response = response.content.decode('utf8')
		self.assertIn(test, html_response)

	def test_post_error_and_render_result(self):
		test = 'Test status'
		response_post = Client().post('/', {'status': ''})
		self.assertEqual(response_post.status_code, 200)

		response= Client().get('/Website/')
		html_response = response.content.decode('utf8')
		self.assertNotIn(test, html_response)
		
class NewVisitorTest(unittest.TestCase):
	def setUp(self):
		chrome_options = Options()
		chrome_options.add_argument('--dns-prefetch-disable')
		chrome_options.add_argument('--no-sandbox')
		chrome_options.add_argument('--headless')
		chrome_options.add_argument('disable-gpu')
		service_log_path = "./chromedriver.log"
		service_args = ['--verbose']
		self.browser = webdriver.Chrome('./chromedriver', chrome_options=chrome_options)
		self.browser.implicitly_wait(25)
		super(NewVisitorTest,self).setUp()
		
	def tearDown(self):
		time.sleep(3)
		self.browser.quit()
		
	def test_can_start_a_list_and_retrieve_it_later(self):
		self.browser.get('http://rahmadiantio-web.herokuapp.com/')
		time.sleep(5)
		box = self.browser.find_element_by_name('status')
		box.send_keys('coba coba')
		box.submit()
		time.sleep(5)
		self.assertIn('coba coba', self.browser.page_source)
		time.sleep(10)
		
	def test_check_titte(self):
		self.browser.get('http://rahmadiantio-web.herokuapp.com/')
		time.sleep(5)
		self.assertEqual('Story 6', self.browser.title)
		self.tearDown()
		
	def test_check_h1(self):
		self.browser.get('http://rahmadiantio-web.herokuapp.com/')
		time.sleep(5)
		welcome = self.browser.find_element_by_id('tulisan').text
		self.assertIn('Hello, Apa kabar?', welcome)
		self.tearDown()
		
	def test_check_class(self):
		self.browser.get('http://rahmadiantio-web.herokuapp.com/')
		time.sleep(5)
		status = self.browser.find_element_by_id('id_status')
		self.assertIn('form-control', status.get_attribute('class'))
		self.tearDown()
		
	def test_check_class_tombol(self):
		self.browser.get('http://rahmadiantio-web.herokuapp.com/')
		time.sleep(5)
		tombol = self.browser.find_element_by_id('tombol')
		self.assertIn('btn btn-default', tombol.get_attribute('class'))
		self.tearDown()	
		
if __name__ == '__main__':
	unittest.main(warnings='ignore')