from django import forms
from .models import Status

class formStatus(forms.Form):
	status = forms.CharField(label = 'status', required = True, max_length = 400, widget = forms.TextInput(attrs = {'class' : 'form-control'}))