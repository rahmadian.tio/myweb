from django.conf.urls import url
from django.urls import path, include

from .views import landpage, post, home

urlpatterns = [
	path('home', home),
	path('Website',landpage),
	path('post', post, name="post"),
	path('', landpage)
]